#include "iauthentication.h"

Au_status           registerUser(const char *, const char *);
Au_status           login(const char *, const char *);
Au_status           changePassword(const char *, const char *);
Au_status           removeUser(const char *);
static Au_status    saveNewUserAndPassword(const char *, const char *);
static Au_status    checkLogin(const char *, const char *);
static int          userExist(const char *);
static void         touchFile(void);
static void         substituteFile(void);
static int          sanitizeUser(const char *);
static int          sanitizePassword(const char *);
static void         encode(char *, const char *, unsigned int);
static unsigned int countMaxUsers(void);

/********************/
/* EXTERN FUNCTIONS */
/********************/

Au_status
registerUser(const char *user, const char *password)
{
	touchFile();

	if (!sanitizeUser(user))
		return AU_SIGN_UP_ERROR;
	if (!sanitizePassword(password))
		return AU_SIGN_UP_ERROR;
	if (countMaxUsers() >= MAX_USERS)
		return AU_MAX_USERS_REACHED;
	if (userExist(user))
		return AU_USER_EXIST;

	return saveNewUserAndPassword(user, password);
}

Au_status
login(const char *user, const char *password)
{
	touchFile();

	if (!sanitizeUser(user))
		return AU_AUTHENTICATION_ERROR;
	if (!sanitizePassword(password))
		return AU_AUTHENTICATION_ERROR;

	return checkLogin(user, password);
}

Au_status
changePassword(const char *user, const char *password)
{
	if (removeUser(user) == AU_REMOVE_USER_ERROR)
		return AU_CHANGE_ERROR;
	if (registerUser(user, password) == AU_SIGN_UP_ERROR)
		return AU_CHANGE_ERROR;
	return AU_CHANGE_OK;
}

Au_status
removeUser(const char *user)
{
	int   user_exist;
	char  buffer[SHADOW_SIZE];
	char  tmp_buffer[SHADOW_SIZE];
	char *find_user;
	FILE *login_file;
	FILE *tmp_file;

	touchFile();

	login_file = fopen(LOGIN_FILE, "rw");
	tmp_file = fopen(TMP_LOGIN_FILE, "w");

	user_exist = 0;

	/* Get users and copy to temporary file, if isn't the user to remove */
	/* If user exist, don't copy to temporary file and change flag value */
	while (fgets(buffer, SHADOW_SIZE, login_file)) {
		strcpy(tmp_buffer, buffer);
		find_user = strtok(tmp_buffer, ":");
		if (strcmp(user, find_user))
			fprintf(tmp_file, "%s", buffer);
		else
			user_exist = 1;
	}

	substituteFile();

	fclose(login_file);
	fclose(tmp_file);

	return (user_exist) ? AU_REMOVE_USER_OK : AU_REMOVE_USER_ERROR;
}

/********************/
/* STATIC FUNCTIONS */
/********************/

static Au_status
saveNewUserAndPassword(const char *user, const char *password)
{
	int           i;
	unsigned int  sha256_length;
	char          string_encode[SHADOW_SIZE];
	FILE         *login_file;

	login_file = fopen(LOGIN_FILE, "a+b");

	if (!login_file)
		return AU_ERROR;	
	
	/* get size of sha256 length */
	sha256_length = gcry_md_get_algo_dlen(GCRY_MD_SHA256);
	/* encode using HMAC sha256 */
	encode(string_encode, password, sha256_length);
	/* encode using HMAC sha256 - N iterations */
	for (i = 0; i < ITERATIONS; i++)
		encode(string_encode, string_encode, sha256_length);

	/* write on file */
	fprintf(login_file, "%s:%s:\n", user, string_encode);

	fclose(login_file);

	return AU_SIGN_UP_OK;
}

static Au_status
checkLogin(const char *user, const char *password)
{
	int           i;
	unsigned int  sha256_length;
	char          string_encode[SHADOW_SIZE];
	char          buffer[SHADOW_SIZE];
	char         *token;
	FILE         *login_file;

	login_file = fopen(LOGIN_FILE, "r");

	/* get size of sha256 length */
	sha256_length = gcry_md_get_algo_dlen(GCRY_MD_SHA256);
	/* encode using HMAC sha256 - first iteration */
	encode(string_encode, password, sha256_length);
	/* encode using HMAC sha256 - N iterations */
	for (i = 0; i < ITERATIONS; i++)
		encode(string_encode, string_encode, sha256_length);

	while (fgets(buffer, sizeof(buffer), login_file)) {
		token = strtok(buffer, ":");
		if (!strcmp(user, token)) {
			token = strtok(NULL, ":");
			if (!strcmp(string_encode, token))
				return AU_AUTHENTICATION_OK;
		}
	}

	fclose(login_file);

	return AU_AUTHENTICATION_ERROR;	
}

static int
userExist(const char *user)
{
	int   user_exist;
	char  buffer[SHADOW_SIZE];
	char  tmp_buffer[SHADOW_SIZE];
	char *find_user;
	FILE *login_file;

	login_file = fopen(LOGIN_FILE, "r");

	user_exist = 0;

	/* Identify if user already exist */
	while (fgets(buffer, SHADOW_SIZE, login_file)) {
		strcpy(tmp_buffer, buffer);
		find_user = strtok(tmp_buffer, ":");
		if (strcmp(user, find_user))
			continue;
		else
			user_exist = 1;
	}

	fclose(login_file);

	return (user_exist) ? 1 : 0;
}

/************************** Create and substitute file ************************/

static void
touchFile(void)
{
	char touch_file[FILE_NAME_SIZE];

	/* If file doesn't exist, create it */
	snprintf(touch_file, FILE_NAME_SIZE, "%s %s", "touch", LOGIN_FILE);
	system(touch_file);
}

static void
substituteFile(void)
{
	char substitute_file[FILE_NAME_SIZE];

	/* Substitute temporary file to new login file */
	snprintf(substitute_file, FILE_NAME_SIZE, "%s %s %s", "mv", 
	         TMP_LOGIN_FILE, LOGIN_FILE);
	system(substitute_file);
}

/****************************** Sanitize Input *******************************/

static int
sanitizeUser(const char *user)
{
	regex_t regex;
	char    regex_string[MAX_LEN_USER] = ".*[A-Z].*";

	/* test length of user */
	if (strlen(user) < MIN_LEN_USER || strlen(user) > MAX_LEN_USER)
		return 0;

	/* compile regex; return 0 on success */
	if (regcomp(&regex, regex_string, REG_EXTENDED))
		return 0;

	/* execute regular expression; return 0 on success */
	if (regexec(&regex, user, 0, NULL, 0) != 0)
		return 0;

	/* free regex */
	regfree(&regex);

	return 1;
}

static int
sanitizePassword(const char *password)
{
	regex_t regex;
	char    regex_string[192] =
	"(.*[A-Z].*[0-9].*[!@#$%&*].*)|(.*[A-Z].*[!@#$%&*].*[0-9].*)|"
	"(.*[0-9].*[A-Z].*[!@#$%&*].*)|(.*[0-9].*[!@#$%&*].*[A-Z].*)|"
	"(.*[!@#$%&*].*[A-Z].*[0-9].*)|(.*[!@#$%&*].*[0-9].*[A-Z].*)";

	/* test length of password */
	if (strlen(password) < MIN_LEN_PW || strlen(password) > MAX_LEN_PW)
		return 0;

	/* compile regular expression */
	if (regcomp(&regex, regex_string, REG_EXTENDED))
		return 0;
	
	/* execute regular expression */
	if (regexec(&regex, password, 0, NULL, 0) != 0)
		return 0;

	/* free regex */
	regfree(&regex);

	return 1;
}


/***************************** Encode algorithm *******************************/

static void
encode(char *string_encode, const char *password, unsigned int sha256_length)
{
	unsigned int   i;
	char           new_password[128];
	unsigned char *encode_password;
	gcry_md_hd_t   h;

	/* include salt */
	strncpy(new_password, password, sha256_length);
	new_password[63] = '\0';
	strncat(new_password, SALT, sha256_length);

	/* init context */
	gcry_md_open(&h, GCRY_MD_SHA256, GCRY_MD_FLAG_HMAC);
	/* insert mac key */
	gcry_md_setkey(h, MAC_KEY, sha256_length);
	/* generate hash */
	gcry_md_write(h, new_password, strlen(new_password));
	/* get result */
	encode_password = gcry_md_read(h, GCRY_MD_SHA256);

	/* convert encode password to string */
	for (i = 0; i < sha256_length; i++)
		sprintf(string_encode+(i*2), "%02x", encode_password[i]);
}

static unsigned int
countMaxUsers(void)
{
	int   count;
	char  buffer[1024];
	FILE *login_file;

	login_file = fopen(LOGIN_FILE, "r");

	count = 0;
	while (fgets(buffer, SHADOW_SIZE, login_file))
		count++;

	fclose(login_file);

	return count;
}
