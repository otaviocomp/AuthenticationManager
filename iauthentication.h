#ifndef IAUTHENTICATION_H
#define IAUTHENTICATION_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <regex.h>
#include <sys/stat.h>
#include <gcrypt.h>

#define MIN_LEN_USER   8
#define MAX_LEN_USER   32
#define MIN_LEN_PW     8
#define MAX_LEN_PW     32
#define MAX_USERS      5
#define FILE_NAME_SIZE 32
#define SHADOW_SIZE    256
#define ITERATIONS     6947
#define MAC_KEY        "24437c1cbc0ceb94e5d5b1bdae2706f"
#define SALT           "40fde96ec89576607327d51ef7fb81fa600593d15c1d6b9e525310e65px98z13"
#define ADMIN_USER     "AdminUser"
#define ADMIN_PASSWORD "b393777103cf26d1cbd0f51ef9398b59daabb931bfb614b7d0a82bb00ca1fef8"

#define LOGIN_FILE     "shadow"
#define TMP_LOGIN_FILE "newshadow"

/* Authentication system status return */
typedef enum authentication_status {
	AU_AUTHENTICATION_OK    = 0,
	AU_AUTHENTICATION_ERROR = 1,
	AU_SIGN_UP_OK           = 2,
	AU_SIGN_UP_ERROR        = 3,
	AU_REMOVE_USER_OK       = 4,
	AU_REMOVE_USER_ERROR    = 5,
	AU_USER_EXIST           = 6,
	AU_MAX_USERS_REACHED    = 7,
	AU_CHANGE_OK            = 8,
	AU_CHANGE_ERROR         = 9,
	AU_ERROR                = 99
} Au_status;

/********************/
/* EXTERN FUNCTIONS */
/********************/

Au_status registerUser(const char *user, const char *password);
Au_status login(const char *user, const char *password);
Au_status changePassword(const char *user, const char *password);
Au_status removeUser(const char *user);

#ifdef __cplusplus
}
#endif

#endif
