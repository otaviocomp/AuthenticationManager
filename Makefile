# Binary name
BIN     = login
# PATHS
DESTBIN = /usr/local/bin
# FLAGS
CFLAGS     = -Wall -Wextra -Werror -O3 -march=native -std=c99 -pedantic
LDFLAGS    = -lgcrypt -lgpg-error

# Files
DEPS = iauthentication.h
SRC  = $(wildcard *.c)
OBJ  = $(SRC:.c=.o)

# Rules
.c.o:
	cc $(CFLAGS) -c $< -o $@

$(BIN): $(OBJ) $(DEPS)
	cc $(OBJ) $(LDFLAGS) -o $@

install: $(BIN)
	mkdir -p $(DESTBIN)
	cp -f $(BIN) $(DESTBIN)
	chmod 755 $(DESTBIN)/$(BIN)

uninstall:
	rm -f $(DESTBIN)/$(BIN)

clean:
	rm -f $(BIN) $(OBJ) shadow

.PHONY: install uninstall clean 
