#include <stdlib.h>
#include <getopt.h>
#include <assert.h>

/* Module interfaces */
#include "iauthentication.h"

static void test(void);
static void menu(void);

int
main(int argc, char **argv)
{
	int opt;
	char user[MAX_LEN_USER];
	char password[MAX_LEN_PW];

	if (argc <= 1)
		menu();

	while ((opt = getopt(argc, argv, "l:s:r:acwt")) != -1) {
		switch (opt) {
			case 'l':
				strncpy(user, optarg, MAX_LEN_USER - 1);
				break;
			case 's':
				strncpy(password, optarg, MAX_LEN_PW - 1);
				break;
			case 'r':
				printf("%d\n", removeUser(optarg));
				break;
			case 'a':
				printf("%d\n", login(user, password));
				break;
			case 'c':
				printf("%d\n", changePassword(user, password));
				break;
			case 'w':
				printf("%d\n", registerUser(user, password));
				break;
			case 't':
				test();
				break;
			case '?':
				printf("unknown option: %c\n", optopt);	
				break;
		}
	}
	return EXIT_SUCCESS;
}

static void
menu(void)
{
	puts("Options:");
	puts("-l: username");
	puts("-s: password");
	puts("-r: remove user");
	puts("-a: login with registered username");
	puts("-c: change password of registered user");
	puts("-w: register new user");
	puts("\nExample: ./login -l <username> -s <password> -a");
}

static void
test(void)
{
	int i;

	/* -------------------- Inputs -------------------------- */

	const char *user_valid[] = {"UmNovoUsuario", "UsuarioNovo",
	"AnotherUser", "UserAnother", "OtherUser"};

	const char *user_invalid[] = {"umnovousuario", "", "user", "OA", "alkjdfls"};

	const char *password_valid[] = {"SenhaMarot@123", "SenhaMarota@123",
	"NovaSenha@123", "Uma@Senha1Qualquer", "Another@Senha1"};

	const char *password_invalid[] = {"", "SenhaMarot@", "senhaqualquer",
	"NovaSenha", "alkjdf123123"};

	/* -------------------- signUP user Fail -------------------- */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_SIGN_UP_ERROR == registerUser(user_invalid[i], password_valid[i]));

	/* -------------------- signUP password Fail ---------------- */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_SIGN_UP_ERROR == registerUser(user_valid[i], password_invalid[i]));

	/* -------------------- signUP user Success ----------------- */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_SIGN_UP_OK == registerUser(user_valid[i], password_valid[i]));

	/* -------------------- Max users --------------------------- */

	assert(AU_MAX_USERS_REACHED == registerUser("Usuariozinho", "Senhaz0nh@"));

	/* -------------------- Success login ----------------------- */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_AUTHENTICATION_OK == login(user_valid[i], password_valid[i]));

	/* -------------------- Fail login -------------------------- */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_AUTHENTICATION_ERROR == login(user_valid[i], password_invalid[i]));

	/* -------------------- Remove users fail ------------------- */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_REMOVE_USER_ERROR == removeUser(user_invalid[i]));

	/* -------------------- Remove users ------------------------ */

	for (i = 0; i < MAX_USERS; i++)
		assert(AU_REMOVE_USER_OK == removeUser(user_valid[i]));

	/* -------------------- signUP same user -------------------- */

	assert(AU_SIGN_UP_OK == registerUser(user_valid[0], password_valid[0]));
	assert(AU_USER_EXIST == registerUser(user_valid[0], password_valid[1]));
	assert(AU_REMOVE_USER_OK == removeUser(user_valid[0]));

	puts("All tests passed!");

	return;
}
